<!DOCTYPE html>
<html lang="en-GB">

	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="@yield('pageDescription', $siteDescription)" />

		<title>{{$siteName}} @yield('pageTitle')</title>

		<link rel="shortcut icon" href="@url('assets/images/favicon.ico')" type="image/x-icon" sizes="16x16 32x32"/>
		<link rel="stylesheet" href="@url('assets/css/spectre.min.css')" />
		<link rel="stylesheet" href="@url('assets/css/spectre-icons.min.css')" />

		<link rel="stylesheet" href="@url('assets/css/all.css')" />
	</head>

	<body>

		<div class="container">
			<div class="columns">
				<div class="column col-12"><section><h2>{{$siteName}}</h2></section></div>
			</div>
		</div>

		<div class="container">

			<div class="columns col-oneline">
				<div class="column col-6">@yield('body')</div>
				<div class="column col-6">
					<nav>
						<div class="nav-menu">
							<div class="container">
								<div class="columns">
									<div class="column col-3">
										<ul class="tab tab-block">
											<li class="tab-item"><a href="@url('/')" class="btn btn-link">Home</a></li>
											<li class="tab-item"><a href="@url('about')" class="btn btn-link">About</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</nav>
					<div class="right-side">@include('_includes.sidebar')</div>
				</div>
			</div>

		</div>

			<div class="container">
				<div class="columns">
					<div class="column col-12">
					    <footer>
						    <div id="footer" class="small">All content copyright © <time>{{ $footerDate }}</time> {{ $siteName }}. Unauthorized reproduction or recreation prohibited.</div>
						</footer>
					</div>
				</div>
			</div>

	</body>

</html>
