<section><h3>Recent Updates</h3><aside>

Vivamus pretium mauris eu volutpat tincidunt.
<br>
Nunc id nulla ac ex ultrices convallis.
<br /><br />

<div class="popover popover-bottom">
  <form style="display: inline" action="@url('about/')" method="get"><button class="btn btn-primary">About</button></form>
  <div class="popover-container">
    <div class="card">
      <div class="card-header">
        <h3>Some title</h3>
      </div>
      <div class="card-body">
       Donec porttitor imperdiet dolor vel sollicitudin.
      </div>
      <div class="card-footer"><i class="icon icon-message"></i>
      </div>
    </div>
  </div>
</div>

<br /><br />
Quisque luctus velit at hendrerit consectetur.
<br />
Donec a quam nec velit ullamcorper luctus.
<br /><br /><hr /><br />

<div class="container">
  <div class="columns">
    <div class="column col-6">
		<div class="card">
			<div class="card-image"><img src="@url('assets/images/jack.png')" class="img-responsive" alt="An image of the Union Jack flag"/></div>
			<div class="card-header">
				<div class="card-title h5">A Title</div>
				<div class="card-subtitle text-gray">A line of subtitle text</div>
			</div>
			<div class="card-body">Quisque luctus velit at hendrerit consectetur.</div>
			<div class="card-footer"><form style="display: inline" action="https://gitlab.com/TW3/katana-example" method="get"><button class="btn btn-success">Source Code</button></form></div>
		</div>
	</div>
  </div>
</div>

<br />
<span></span>
<br />
<br />
</aside></section>
