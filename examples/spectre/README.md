
# katana spectre theme

![spectre theme](./spectre.png)

This theme replaces the default theme shipped with Katana and incorporates the excellent [spectre][c29fe79d] css framework.
The theme is designed as a starting point for developing your own site using spectre.

Spectre's documentation can be found [here][5b75747b].

  [c29fe79d]: https://picturepan2.github.io/spectre/index.html "spectre"
  [5b75747b]: https://picturepan2.github.io/spectre/getting-started.html "docs"

## How to use

- Replace the folder **web/content** with the **content** folder found in the directory containing this file
- Replace (and optionally tailor to your own needs) the file **web/config.php** with the **config.php** file found in the same directory as this file
- `git add * && git commit -a`
- `git push`

Once the pipelines have successfully completed, visit your project's Gitlab pages url and you will be able to view the theme.
