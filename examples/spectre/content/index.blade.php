@extends('_includes.base')

@section('body')

    <div class="welcome">
        <div class="wrapper">
            <h1>Katana spectre css website test!</h1>
                <header>
                    <span>{{ $siteDescription }}</span>
                </header>
        </div>
    </div>

    <div class="left-side"><br />
        <main>
@markdown

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et purus erat. Morbi sollicitudin mattis varius. Fusce tempus quam lectus, id molestie orci faucibus ut. Integer consectetur ante at ex aliquet tincidunt. Sed sagittis diam eu fringilla eleifend.

[Learn about the spectre css framework](https://picturepan2.github.io/spectre/getting-started.html).

Morbi a velit arcu. Nam fermentum lorem diam, sed viverra ligula egestas quis. Duis eget lacus vel mi commodo tristique in at felis. Nullam consectetur ante non diam ullamcorper sodales. Sed leo dui, cursus at turpis sit amet, pretium lobortis nunc. Cras suscipit consequat rutrum.

> “In fermentum efficitur ipsum sit amet elementum.“
> ― Anon

Cras sagittis lacus vitae diam dictum cursus. Nullam molestie ex lacus, in rhoncus tortor auctor a. Donec porttitor imperdiet dolor vel sollicitudin.

- Proin vulputate justo nec cursus ultrices.
- Ut dignissim lectus vitae tempus placerat.
- Donec eget ligula id lorem fermentum pulvinar.
- Nulla efficitur massa quis urna semper euismod.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Maecenas sit amet tincidunt enim, at feugiat lorem. Maecenas in neque quis diam molestie suscipit. Nullam a facilisis orci, ut commodo elit. Aliquam aliquam magna sit amet diam tristique, a lacinia ante suscipit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..


@endmarkdown
        </main>
    </div>
@stop