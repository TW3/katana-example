
# katana skeleton theme
![skeleton theme](./skeleton.png)

The most stripped back a theme should be to be useful as a great starting point for an experienced web developer.
The theme has no styles and is ready to have css + js + content added according to your needs.

## How to use

- Replace the folder **web/content** with the **content** folder found in the directory containing this file
- Replace (and optionally tailor to your own needs) the file **web/config.php** with the **config.php** file found in the same directory as this file
- `git add * && git commit -a`
- `git push`

Once the pipelines have successfully completed, visit your project's Gitlab pages url and you will be able to view the theme.
