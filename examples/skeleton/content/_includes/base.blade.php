<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('pageDescription', $siteDescription)">

    <title>{{$siteName}} @yield('pageTitle')</title>

	<link rel="shortcut icon" href="@url('assets/images/favicon.ico')" type="image/x-icon" sizes="16x16 32x32"/>
    <link rel="stylesheet" href="@url('assets/css/all.css')">
</head>

<body>

<nav>
    <div class="wrapper">
        <strong>{{$siteName}}</strong>
        <ul>
            <li><a href="@url('/')">Home</a></li>
        </ul>
    </div>
</nav>

<div class="wrapper m-t-30">
    @yield('body')
</div>

</body>
</html>