
# katana theme examples

This directory contains themes which can be used as a starting point for your web project using Katana.
Once you have your site up and running, this examples directory can be deleted.

## How to install a theme

Please check the readme files inside each theme's directory for instructions.
