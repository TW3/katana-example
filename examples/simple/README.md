
# katana simple theme

![simple theme](./simple.png)

A simple theme for Katana. This theme is pretty much ready to go once you have made some minor changes to suit.

## How to use

- Replace the folder **web/content** with the **content** folder found in the directory containing this file
- Replace (and optionally tailor to your own needs) the file **web/config.php** with the **config.php** file found in the same directory as this file
- `git add * && git commit -a`
- `git push`

Once the pipelines have successfully completed, visit your project's Gitlab pages url and you will be able to view the theme.
