@extends('_includes.base')

@section('body')

    <div class="welcome">
        <div class="wrapper">
            <h1>Katana simple website test!</h1>
            <span>{{ $siteDescription }}</span>
        </div>
    </div>


    <div class="left-side">
        @markdown

		Lorem Ipsum



Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et purus erat. Morbi sollicitudin mattis varius. Fusce tempus quam lectus, id molestie orci faucibus ut. Integer consectetur ante at ex aliquet tincidunt. Sed sagittis diam eu fringilla eleifend. Suspendisse potenti. Sed nec nunc id tellus condimentum vulputate vitae et mi. Proin sit amet lorem et lectus varius laoreet lacinia nec ante. Interdum et malesuada fames ac ante ipsum primis in faucibus.

[More](/about/)...

Morbi a velit arcu. Nam fermentum lorem diam, sed viverra ligula egestas quis. Duis eget lacus vel mi commodo tristique in at felis. Nullam consectetur ante non diam ullamcorper sodales. Sed leo dui, cursus at turpis sit amet, pretium lobortis nunc. Cras suscipit consequat rutrum. Donec a quam nec velit ullamcorper luctus.

        >"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."

Cras sagittis lacus vitae diam dictum cursus. Nullam molestie ex lacus, in rhoncus tortor auctor a. Donec porttitor imperdiet dolor vel sollicitudin. Quisque luctus velit at hendrerit consectetur. Maecenas sit amet tincidunt enim, at feugiat lorem. Maecenas in neque quis diam molestie suscipit. Nullam a facilisis orci, ut commodo elit. Aliquam aliquam magna sit amet diam tristique, a lacinia ante suscipit. In fermentum efficitur ipsum sit amet elementum. 


        - Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        - Donec eget ligula id lorem fermentum pulvinar.
        - Sed consequat tortor vitae nisl laoreet, ut luctus sem euismod.
        - Proin vulputate justo nec cursus ultrices.
        - Ut dignissim lectus vitae tempus placerat.
        - Nulla efficitur massa quis urna semper euismod.

        ---



        @endmarkdown
    </div>

    <div class="right-side">
        @include('_includes.sidebar')
    </div>

@stop