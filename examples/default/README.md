
# katana default theme

![default theme](./default.png)

This is the default theme which ships with Katana. The theme is designed as a starting point for developing your own blog site using spectre.

## How to use

- Replace the folder **web/content** with the **content** folder found in the directory containing this file
- Replace (and optionally tailor to your own needs) the file **web/config.php** with the **config.php** file found in the same directory as this file
- `git add * && git commit -a`
- `git push`

Once the pipelines have successfully completed, visit your project's Gitlab pages url and you will be able to view the theme.
