
# katana-example

An example for Gitlab pages.

View the demo: https://tw3.gitlab.io/katana-example/

With this repo, you can use Gitlab pages and Katana to serve a static website which is tracked by git and automatically deployed by Gitlab CI; as you make changes.
:rocket:

![screenshot](./screenshot.png)

## Katana
(http://themsaid.github.io/katana/)

Simply fork this repository and ensure that CI is enabled on your Gitlab repository (the default.) Next, make your changes and commit them. The first time you commit, the cache is created. From then on your commits deploy very fast (depending upon available runners.)

Edit your site content inside of the **web/content** directory. Make your css edits inside **assets/css/all.css** if you like. Once you are up and running, your repo only needs to contain:

- The **web** folder and it's contents
- The file **.gitlab-ci.yml**

All the other files and folders can be removed. And it's recommended that you do so. The rest is down to you!

If you decide that you want to only deploy upon trigger proc, schedule activation or tag creation, un-comment both instances of the `only:` and `refs:` sections inside **.gitlab-ci.yml** and choose the way you want to deploy.

Within your project's Gitlab settings, it might be wise to lower the CI / CD Settings timeout value from 1h to 14m, for example.

Be patient, the very first time the pipelines sucessfully go through. At peak times it may take Gitlab upto or approximately 10 minutes to properly setup Gitlab pages for you in the background.
To find out your project's pages url, visit `https://gitlab.com/YOUR_USER_ACCOUNT_NAME/YOUR_PROJECT_NAME/pages` when signed in.

For developers: Katana's source code can be found at https://github.com/themsaid/katana and information about the Blade templating engine can be found here https://laravel.com/docs/5.2/blade

## Creating A Local Test Environment

The following steps can be followed to setup a testing environment using windows:

- Download the php for windows zip archive from https://windows.php.net/download/ (**VC15 x64 Thread Safe** is probbably the version most people want)
- Download **composer.phar** from https://getcomposer.org/download/ using the section of the page titled **Manual Download** (**1.8.5** - being current as of **2019-04-09**)
- Create the directory **test**
- Copy the file **composer.phar** & the contents of **php-x.x.x-Win32-VC15-x64.zip** into the folder "test"
- Open a command prompt window (cmd) and move into the directory named test
- Copy **php.ini-production** to **php.ini**
- Issue the following powershell commands:

`powershell "(Get-Content "php.ini").replace(';extension=openssl', 'extension=.\ext\php_openssl.dll') | Set-Content "php.ini""`

`powershell "(Get-Content "php.ini").replace(';extension=curl', 'extension=.\ext\php_curl.dll') | Set-Content "php.ini""`

`powershell "(Get-Content "php.ini").replace(';extension=mbstring', 'extension=.\ext\php_mbstring.dll') | Set-Content "php.ini""`

- Next, create the project at the prompt using:

`php composer.phar create-project themsaid/katana katana`

- Move into the katana directory
- Build the site from the prompt using:

`..\php katana build`

- Start php's built in web server at the prompt using:

`..\php -S localhost:8080 -t public\`

- Visit http://localhost:8080/ using the web browser of your choice to view the site

The steps are similar for Linux or MacOS users although using a package manager to obtain composer and php is recommended instead. And paths will obviously differ.

### Notes

You can use a full path to run the php web server like so:

`SET WORK_DIR=D:\sandbox`

`%WORK_DIR%\test\php -S localhost:8080 -t %WORK_DIR%\test\katana\public`

- You can stop the php webserver from the command prompt using the **Ctrl + C** key combination.
- Every time you make changes to the content directory, you must run: `..\php katana build` to build the site before refreshing the web browser.
- Once you are happy with the code in your test environment, simply replace everything inside the contents folder in your git repo with the contents of your development repo.

Watch out for `'base_url' => '/',` and `'base_url' => '/placeholder',` inside the **config.php** file if you modify it.
When testing, you will need **/** and not **/placeholder** but when using gitlab CI, you will need to ensure that `'base_url' => '/placeholder'` is in place.
